script_name('Lista SAPD')
script_version('1.05')
script_author('Xaero')

------------------------------------------------------------------------------------------------------------------------
-- Librerias requeridas
local bNotf, notf = pcall(import, "myimgui_notf.lua")
local cjson = require"cjson"
local lanes = require('lanes').configure()
local effil = require"effil"
local ffi = require 'ffi'
local str = ffi.string
local fa = require('fAwesome6')
local imgui = require 'mimgui'
local encoding = require('encoding') 
local u8 = encoding.UTF8
encoding.default = 'ISO-8859-1'
local requests = require 'requests'
requests.http_socket, requests.https_socket = http, http
local sampev = require 'samp.events'
------------------------------------------------------------------------------------------------------------------------
-- Info
local listo = true
local logged = false
local recordar = imgui.new.bool()
local renderSus = imgui.new.bool()
local renderWindow = imgui.new.bool()
local renderBuscados = imgui.new.bool()
local sizeX, sizeY = getScreenResolution()
local inprocess = false
local ultimaactu 
local ultimaactu2
local sudo = false
local nombres = ""
local actufech = ""
local susnam = imgui.new.char[256]("")
local susraz = imgui.new.char[256]("")
local nivelb = imgui.new.int()
local niveb = imgui.new.bool()
local nive = imgui.new.bool()
local pw = imgui.new.char[15]("")
local tag = "{82807f}[Lista PD] {ffffff}"
local nivelbusqueda = {
	"Baja",
	"Media",
	"Alta",
	"Maxima"
	}
fileS = "moonloader/LISTASAPD/lista.sapd"
local notifytable = {}
notifydb = io.open(fileS, "a+")
tempList = {}

for todo in notifydb.lines(notifydb) do
    local nombre, razon, prioridad, fecha = todo:match("^(%S+)%s*(.-)%[(%d)%]%[(%d-)%]$")
	if nombre and razon and prioridad and fecha then
    table.insert(notifytable, {
		name = nombre:lower(),
		reason = razon,
		priority = prioridad,
		date = fecha
    })
	end
end
io.close(notifydb)	

------------------------------------------------------------------------------------------------------------------------
-- Funciones Mimgui

imgui.OnInitialize(function()
	imgui.DarkTheme()
	imgui.GetIO().IniFilename = nil
	local font_config = imgui.ImFontConfig()
	local defGlyph = imgui.GetIO().Fonts.ConfigData.Data[0].GlyphRanges
	font_config.MergeMode = false
	font = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\arialbd.ttf', 15, font_config, defGlyph)
	font2 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\arialbd.ttf', 25, font_config, defGlyph)
	font3 = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(0x14) .. '\\arialbd.ttf', 12, font_config, defGlyph)
	font_config.MergeMode = true
	fa.get_font_data_base85('solid')
    font_config.PixelSnapH = true
    iconRanges = imgui.new.ImWchar[3](fa.min_range, fa.max_range, 0)
    fafont = imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(fa.get_font_data_base85('solid'), 15, font_config, iconRanges)
	logosapd = imgui.CreateTextureFromFile(getWorkingDirectory() .. '/LISTASAPD/logosapd.png')
	xmark = imgui.CreateTextureFromFile(getWorkingDirectory() .. '/LISTASAPD/xmark.png')
end)

local tab = imgui.new.int(1)
tab[0] = 1
local newFrame = imgui.OnFrame(
    function() return renderWindow[0] and not isGamePaused() end,
    function(player)
        imgui.SetNextWindowPos(imgui.ImVec2(sizeX / 2, sizeY / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(300, 300), imgui.Cond.FirstUseEver)
        imgui.Begin("Main Window", renderWindow, imgui.WindowFlags.NoResize + imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoTitleBar)
		if tab[0] == 1 then
			local DL = imgui.GetWindowDrawList()
			local p = imgui.GetCursorScreenPos()
			local size = imgui.ImVec2(170, 170)
			DL:AddImage(logosapd, imgui.ImVec2(p.x + 60,p.y + 40), imgui.ImVec2(p.x + 60 + size.x , p.y + size.y + 40), imgui.ImVec2(0, 0), imgui.ImVec2(1, 1))
			imgui.PushFont(font2)
			imgui.SetCursorPos(imgui.ImVec2((imgui.CalcTextSize('Base de Datos | SAPD').x/2)-85, 5))
			imgui.Text("Base de Datos | SAPD")
			imgui.PopFont()
			imgui.PushFont(font)
			imgui.SetCursorPos(imgui.ImVec2((imgui.CalcTextSize('Ingrese la Clave de Seguridad').x/2)-40, 223))
			imgui.Text("Ingrese la Clave de Seguridad")
			imgui.PopFont()
			imgui.SetCursorPos(imgui.ImVec2(90, 245))
			imgui.PushItemWidth(120)
			if imgui.InputTextWithHint('##Clave de Ingreso',"Clave de Ingreso", pw, 150,imgui.InputTextFlags.EnterReturnsTrue+imgui.InputTextFlags.Password) then --if imgui.CustomInputTextWithHint('##password', pw, 'Clave de Ingreso', 1.2, 120, imgui.ColorConvertFloat4ToU32(imgui.ImVec4(247/255, 247/255, 247/255,1)), true, true)
				pass = str(pw)
				if tostring(enc(pass)) == "U0EyMDI0UEREQg==" then
					imgui.StrCopy(pw, '')
					renderBuscados[0] = true
					renderWindow[0] = false
					sudo = false
				elseif tostring(enc(pass)) == "VVBTQVBEMjREQg==" then
					imgui.StrCopy(pw, '')
					renderBuscados[0] = true
					renderWindow[0] = false
					logged = true
					sudo = true
				else
					imgui.StrCopy(pw, '')
					tab[0] = 3
				end
			end
			imgui.PopItemWidth()
			imgui.SetCursorPos(imgui.ImVec2(5, 270))
			imgui.Checkbox("Recordar sesion", recordar)
		elseif tab[0] == 3 then
			local DL = imgui.GetWindowDrawList()
			local p = imgui.GetCursorScreenPos()
			local size = imgui.ImVec2(100, 100)
			DL:AddImage(xmark, imgui.ImVec2(p.x + 94,p.y + 20), imgui.ImVec2(p.x + 94 + size.x , p.y + size.y + 20), imgui.ImVec2(0, 0), imgui.ImVec2(1, 1))
			imgui.PushFont(font)
			imgui.SetCursorPos(imgui.ImVec2(149-(imgui.CalcTextSize('Clave Incorrecta').x/2), 140))
			imgui.Text("Clave Incorrecta")
			imgui.PopFont()
			imgui.SetCursorPos(imgui.ImVec2(63, 170))
			imgui.Text("Intentalo de nuevo o solicita la \n  contraseña a un compañero.")
			imgui.SetCursorPos(imgui.ImVec2(115, 230))
			if imgui.Button("Volver", imgui.ImVec2(70, 30)) then
				tab[0] = 1
			end
		end
        imgui.End()                 
    end)

function notnil(arg); if arg then return true; elseif not arg then return false end; end

local newFrameB = imgui.OnFrame(
    function() return renderBuscados[0] and not isGamePaused() end,
    function(player)
		imgui.SetNextWindowPos(imgui.ImVec2(sizeX / 2, sizeY / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
        imgui.SetNextWindowSize(imgui.ImVec2(450, 390), imgui.Cond.FirstUseEver)
        imgui.Begin("SUB Window", renderBuscados, imgui.WindowFlags.NoResize + imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoTitleBar)
		local DL = imgui.GetWindowDrawList()
		local p = imgui.GetCursorScreenPos()
		imgui.PushFont(font2)
		imgui.Text("Base de Datos | SAPD")
		imgui.PopFont()
		imgui.SameLine(imgui.GetCursorPos().x + 250)
		imgui.PushFont(font3)
		imgui.SetCursorPosY(19)
		imgui.Text("Ultima Actualización:".. actufech or "XX:XX:XX AM")
		imgui.PopFont()
		DL:AddLine(imgui.ImVec2(p.x-7,p.y+27), imgui.ImVec2(p.x+455,p.y+27), imgui.ColorConvertFloat4ToU32(imgui.ImVec4(1, 1, 1, 1)),3)
		
		imgui.SetCursorPosY(37)
		if sudo then
			imgui.SetCursorPosY(55)
			imgui.SetCursorPosX(imgui.GetWindowSize().x - 52)
			imgui.PushFont(fafont)
			if imgui.RippleButton(fa.ROTATE_RIGHT.."" ,imgui.ImVec2(35, 35), 1.5, 16) then
				getgist()
			end
			imgui.SetCursorPosY(55)
			imgui.SetCursorPosX(imgui.GetWindowSize().x - 114)
			if imgui.RippleButton(fa.USER_TAG..'', imgui.ImVec2(35,35), 1.5, 16) then
				renderSus[0] = not renderSus[0]
			end
			imgui.SetCursorPosY(55)
			imgui.SetCursorPosX(imgui.GetWindowSize().x - 176)
			if imgui.RippleButton(""..fa.FILE_PLUS.."" ,imgui.ImVec2(35, 35), 1.5, 16,nil,true) then
				imgui.OpenPopup('Agregar sospechoso')
			end
			imgui.PopFont()
		else
			imgui.SetCursorPosY(55)
			imgui.SetCursorPosX(imgui.GetWindowSize().x - 87)
			imgui.PushFont(fafont)
			if imgui.RippleButton(fa.ROTATE_RIGHT.."" ,imgui.ImVec2(35, 35), 1.5, 16) then
				getgist()
			end
			imgui.SetCursorPosY(55)
			imgui.SetCursorPosX(imgui.GetWindowSize().x - 154)
			if imgui.RippleButton(fa.USER_TAG..'', imgui.ImVec2(35,35), 1.5, 16) then
				renderSus[0] = not renderSus[0]
			end
			imgui.PopFont()
		end
		imgui.SetCursorPosX(5)
		imgui.SetCursorPosY(40)
		imgui.BeginChild('##subex', imgui.ImVec2(250, 100), true)
		local myid = select(2, sampGetPlayerIdByCharHandle(playerPed))
		local mynick = sampGetPlayerNickname(myid)
		local pname, papell = string.match(mynick,"(%a+)_(%a+)")
		imgui.Columns(3) 
		imgui.Text(u8'Nombre') imgui.SetColumnWidth(-1, 100)
		imgui.NextColumn()
		imgui.Text(u8'Apellido') imgui.SetColumnWidth(-1, 100)
		imgui.NextColumn()
		imgui.Text(u8'SUDO') imgui.SetColumnWidth(-1, 50)
		imgui.Columns(1)
		imgui.Separator() 	
		imgui.Columns(3) 
		imgui.Text(pname or "Xxx") imgui.SetColumnWidth(-1, 100)
		imgui.NextColumn()
		imgui.Text(papell or "Xxx") imgui.SetColumnWidth(-1, 100)
		imgui.NextColumn()
		imgui.Text(sudo and 'Si' or 'No') imgui.SetColumnWidth(-1, 50)
		imgui.Columns(1)
		imgui.Separator()
		imgui.Text('La siguiente informacion es confidencial.') 
		imgui.EndChild()
		if imgui.BeginPopupModal('Agregar sospechoso', _, imgui.WindowFlags.NoResize) then
			imgui.SetWindowSizeVec2(imgui.ImVec2(200, 175))
			imgui.SetCursorPos(imgui.ImVec2(6, 30))
			imgui.PushItemWidth(110)
			if imgui.InputTextWithHint(u8"##", "Nombre o ID", susnam, imgui.InputTextFlags.NoHorizontalScroll) then 
				nam = str(susnam)
			end
			imgui.PopItemWidth()
			imgui.SetCursorPos(imgui.ImVec2(6, 60))
			imgui.PushItemWidth(110)
			if imgui.InputTextWithHint(u8"####", "Razon", susraz, imgui.InputTextFlags.NoHorizontalScroll) then 
				raz = str(susraz)
			end
			imgui.PopItemWidth()
			imgui.SetCursorPosX(6)
			if imgui.RippleButton("Agregar!" ,imgui.ImVec2(110, 30), 1.5, 3,nil,false) then
				raz = str(susraz)
				nam = str(susnam)
				if checknumber(nam) and tonumber(nam) >= 0 and tonumber(nam) <= 999 then
					if sampIsPlayerConnected(nam) then
						nam = sampGetPlayerNickname(nam)
					end
				end
				addnotify(nam.." "..raz, nivelb[0])
				updategist()
				imgui.CloseCurrentPopup()
				imgui.StrCopy(susraz, "")
				imgui.StrCopy(susnam, "")
			end
			imgui.SetCursorPosX(6)
			imgui.SetCursorPosY(125)
			if imgui.RippleButton("Cancelar" ,imgui.ImVec2(110, 30), 1.5, 3,nil,false) then
				imgui.CloseCurrentPopup()
				imgui.StrCopy(susraz, "")
				imgui.StrCopy(susnam, "")
			end
			imgui.SetCursorPos(imgui.ImVec2(120, 30))
			imgui.Text("Prioridad:")
			for i = 1, 4 do
				imgui.SetCursorPos(imgui.ImVec2(120, 15+(30*i)))
				local tex = ""
				--if i == 1 then tex = "Baja" elseif i == 2 then tex = "Media"elseif i == 3 then tex = "Alta" elseif i == 4 then tex = "Maxima" end
				if imgui.RadioButtonIntPtr(nivelbusqueda[i],nivelb,i) then
					nivelb[0] = i
				end
			end
			imgui.EndPopup()
		end
		imgui.SetCursorPosX(5)
		imgui.SetCursorPosY(111)
		imgui.BeginChild('##subsus', imgui.ImVec2(440, 270), true)
		imgui.Columns(4) 
		imgui.Text(u8'  Nombre_Apellido') imgui.SetColumnWidth(-1, 120)
		imgui.NextColumn()
		imgui.Text(u8'              Razon') imgui.SetColumnWidth(-1, 145)
		imgui.NextColumn()
		imgui.Text(u8' Prioridad') imgui.SetColumnWidth(-1, 65)
		imgui.NextColumn()
		imgui.Text(u8'         Fecha') imgui.SetColumnWidth(-1, 100) 
		imgui.Columns(1)
		imgui.Separator() imgui.SameLine() imgui.Separator()			
		for k, v in pairs(notifytable) do
			v.name = v.name:gsub("(%d)","")
			if v.name ~= "" then
				local priori = ""
				if tonumber(v.priority) == 1 then priori = "  Baja" 
				elseif tonumber(v.priority) == 2 then priori = " Media" 
				elseif tonumber(v.priority) == 3 then priori = "  Alta" 
				elseif tonumber(v.priority) == 4 then priori = "Maxima" end
				local wrazon = textWrap( v.reason,25, nil, "" )
				imgui.Columns(4) 
				imgui.Text(mayusculizador(v.name))
				if imgui.IsItemClicked() then
					if sudo then
					nombres = v.name
					imgui.OpenPopup('Eliminar Sospechoso')
					end
				end 
				if isonline(mayusculizador(v.name)) then
				imgui.SameLine()
				imgui.TextColored(imgui.ImVec4(178/255, 222/255, 39/255, 1), "@")
				end
				imgui.SetColumnWidth(-1, 120)
				imgui.NextColumn()
				imgui.Text(wrazon .. " ") imgui.SetColumnWidth(-1, 145) 
				imgui.NextColumn()
				imgui.Text("  "..priori .. " ") imgui.SetColumnWidth(-1, 65) 
				imgui.NextColumn()
				imgui.Text("  "..os.date("%H:%M %d/%m/%y",v.date) .. " ") imgui.SetColumnWidth(-1, 100) 
				imgui.Columns(1)
				imgui.Separator()
				end
		end
		if imgui.BeginPopupModal('Eliminar Sospechoso', _, imgui.WindowFlags.NoResize) then
			imgui.SetWindowSizeVec2(imgui.ImVec2(180, 100))
			imgui.Text("¿Esta seguro de eliminar a \n"..mayusculizador(nombres).." de la lista?")
			imgui.SetCursorPosX(27)
			imgui.SetCursorPosY(imgui.GetWindowSize().y - 31)
			if imgui.Button(u8'Eliminar', imgui.ImVec2(60, 25)) then
				removenotify(nombres)
				updategist()
				imgui.CloseCurrentPopup()
			end
			imgui.SameLine()
			if imgui.Button(u8'Cancelar', imgui.ImVec2(60, 25)) then
				imgui.CloseCurrentPopup()
			end
			imgui.EndPopup()
		end
		imgui.EndChild()
		imgui.End()       
end)

local newFrameS = imgui.OnFrame(
    function() return renderSus[0] and not isGamePaused() end,
    function(player)
	    local X, Y = getScreenResolution()
		imgui.PushStyleVarFloat(imgui.StyleVar.WindowRounding, 0)
 		imgui.SetNextWindowSize(imgui.ImVec2(200, 100), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(X / 2 + 300, Y / 2 - 100), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
		imgui.Begin("Sospechosos en la ciudad", renderSus, imgui.WindowFlags.NoCollapse)
		maxPlayerOnline = sampGetMaxPlayerId(false)
		for id = 0, maxPlayerOnline, 1 do
			if sampIsPlayerConnected(id) then
				name = sampGetPlayerNickname(id)
				for k, v in pairs(notifytable) do
					if name:lower() == v.name then
					imgui.Text("  "..name .. " (" .. id .. ")")
					end
				end
			end
		end
		if not renderBuscados[0] and not renderWindow[0] then
			player.HideCursor = true
		end
		imgui.SetCursorPosY(imgui.GetWindowSize().y - 30)
		if imgui.Button("Cerrar") or not renderSus[0] then
		  renderSus[0] = false
		end
		imgui.End()
		imgui.PopStyleVar(1)
    end)

------------------------------------------------------------------------------------------------------------------------
-- Funcion base

function main()
	while not isSampAvailable() do wait(100) end

	--autoupdate("Link a un Json para comprobar la versión", '{82807f}[Lista SAPD]{ffffff} ' , "Link del repositorio")
    sampAddChatMessage(tag .."Autor: {82807f}Xaero{FFFFFF}. Para abrir la lista usa: {82807f}/listpd{FFFFFF}", -1)

    sampRegisterChatCommand('listpd',  function ()
		if logged and recordar[0] then
			renderBuscados[0] = not renderBuscados[0]
		else
			renderWindow[0] = not renderWindow[0]
		end
    end)
	
    while true do
			
		if ultimaactu == nil then
			ultimaactu = os.time()
			ultimaactu2 = ultimaactu+300
			getgist()
		end
		if os.time() >= ultimaactu2 then
			ultimaactu = os.time()
			ultimaactu2 = ultimaactu+300
			getgist()
			--autoupdate("Link a un Json para comprobar la versión", '{82807f}[Lista SAPD]{ffffff} ' , "Link del repositorio")
		end
		if reload then
			
			notifytable = {}
			notifydb = io.open(fileS, "a+")
			for todo in notifydb.lines(notifydb) do
				local nombre, razon, prioridad, fecha= todo:match("^(%S+)%s*(.-)%[(%d)%]%[(%d-)%]")
				if nombre and razon and prioridad and fecha then
				table.insert(notifytable, {
					name = nombre:lower(),
					reason = razon,
					priority = prioridad,
					date = fecha
				})
				end
			end
			io.close(notifydb)
			reload = false
		end
		if renderWindow[0] then logged = false end
		if renderBuscados[0] and not renderWindow[0] then logged = true end
		if not renderWindow[0] and not recordar[0] then tab[0] = 1 end
        wait(0)
	end
end

------------------------------------------------------------------------------------------------------------------------
-- Funcion principal + API 
function dec(data)
    local b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end

function updategist()
	if sudo then
		notifydb = io.open(fileS, "a+")
		local texto = ""
		for todo in notifydb.lines(notifydb) do
			local nombre, razon, prioridad, fecha = todo:match("^(%S+)%s*(.-)%[(%d)%]%[(%d-)%]")
			if nombre and razon then
			nombre = nombre:gsub("(%d)","")
			texto = texto..nombre.." "..razon.."["..prioridad.."]["..fecha.."]|"
			end
		end
		inprocess = true
		async_http_request(
			"PATCH",
			"https://api.github.com/gists/ID del GIST", {
				headers = {
					Authorization = "AUTH CODE HERE",
					["X-GitHub-Api-Version"] = "2022-11-28"
				},
				data = encodeJson({
					['description'] = "Lista de Sospechosos SAPD 2024 - GX-RP",
					['public'] = false,
					['files'] = { 
						['ListaSospechosos.md'] = { 
							['content'] = texto
						}
					}
				})
			},
			function(response)
				inprocess = false
				print(table_to_string(response))
			end,
			function(err)
				print("Ocurrio un error, intenta de nuevo.")
			end
		)
		io.close(notifydb)	
	end
end

function getgist()
	async_http_request(
		"GET",
		"https://api.github.com/gists/ID del GIST", {
			headers = {
				Authorization = "AUTH CODE HERE"
			}
		},
		function(response)
			local rdata = response.text
			rdata = cjson.decode(rdata)
			local contenido = string.match(table_to_string(rdata), '"content"]="(.-)"')
			if contenido and contenido:find("|") then
				notifydb = io.open(fileS, "w")
				io.output(notifydb)
				io.write(string.gsub(contenido, "|", "\n"))
				io.close(notifydb)
				reload = true
			else
				notifydb = io.open(fileS, "w")
				io.output(notifydb)
				io.write("")
				io.close(notifydb)
				reload = true
			end
			actufech = os.date("%I:%M:%S %p",os.time())
			
		end,
		function(err)
			print("Ocurrio un error, intenta de nuevo.")
		end
	)
end

function table_to_string(tbl)
    local result = "{"
    for k, v in pairs(tbl) do
        if type(k) == "string" then
            result = result.."[\""..k.."\"]".."="
        end
        if type(v) == "table" then
            result = result..table_to_string(v)
        elseif type(v) == "boolean" then
            result = result..tostring(v)
        else
            result = result.."\""..tostring(v).."\""
        end
        result = result..","
    end
    if result ~= "" then
        result = result:sub(1, result:len()-1)
    end
    return result.."}"
end

------------------------------------------------------------------------------------------------------------------------
-- Funciones extra

function textWrap( str, limit, indent, indentFirst )
    limit = limit or 72
    indent = indent or ""
    indentFirst = indentFirst or indent
 
    local here = 1 - #indentFirst
    return indentFirst .. str:gsub( "(%s+)()(%S+)()",
        function( sp, st, word, fi )
            if fi - here > limit then
                here = st - #indent
                return "\n" .. indent .. word
            end
        end
    )
end

local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' 
function enc(data)
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

function onWindowMessage(msg, wparam, lparam)
    if renderWindow[0] or renderBuscados[0] then
		if wparam == 0x1B and renderWindow[0] then -- ESCAPE
            if msg == 0x0100 then -- WM_KEYDOWN
                consumeWindowMessage(true, false)
            end
            if msg == 0x0101 then -- WM_KEYUP
                renderWindow[0] = false
            end
		elseif wparam == 0x1B and renderBuscados[0] then
		    if msg == 0x0100 then -- WM_KEYDOWN
                consumeWindowMessage(true, false)
            end
            if msg == 0x0101 then -- WM_KEYUP
                renderBuscados[0] = false
            end
        end

	end
end

function mayusculizador(nombre)
	local prila, restA, prilb, restB = nombre:match("^(.)(.-)_(.)(.-)$")
	local lminus = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
	local lmayus = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
	local result = ""
	for i = 1, 26 do
		if prila == lminus[i] then
			prila = lmayus[i]
		end
	end
	for i = 1, 26 do
		if prilb == lminus[i] then
			prilb = lmayus[i]
		end
	end
	result = prila..restA.."_"..prilb..restB
	return result
end

function async_http_request(method, url, args, resolve, reject)
	local request_lane = lanes.gen('*', {package = {path = package.path, cpath = package.cpath}}, function()
		local requests = require 'requests'
        local ok, result = pcall(requests.request, method, url, args)
        if ok then
            result.json, result.xml = nil, nil -- cannot be passed through a lane
            return true, result
        else
            return false, result -- return error
        end
    end)
    if not reject then reject = function() end end
    lua_thread.create(function()
        local lh = request_lane()
        while true do
            local status = lh.status
            if status == 'done' then
                local ok, result = lh[1], lh[2]
                if ok then resolve(result) else reject(result) end
                return
            elseif status == 'error' then
                return reject(lh[1])
            elseif status == 'killed' or status == 'cancelled' then
                return reject(status)
            end
            wait(0)
        end
    end)
end

function imgui.RippleButton(text, size, duration, rounding, parent_color, disa)
       
    local function CenterTextFor2Dims(text)
        local width = imgui.GetWindowWidth()
        local calc = imgui.CalcTextSize(text)
        local height = imgui.GetWindowHeight()
        imgui.SetCursorPosX( width / 2 - calc.x / 2 )
        imgui.SetCursorPosY(height / 2 - calc.y / 2)
        imgui.Text(text)
    end

    local function bringVec4To(from, to, start_time, duration)
        local timer = os.clock() - start_time
        if timer >= 0.00 and timer <= duration then
            local count = timer / (duration / 100)
            return imgui.ImVec4(
                from.x + (count * (to.x - from.x) / 100),
                from.y + (count * (to.y - from.y) / 100),
                from.z + (count * (to.z - from.z) / 100),
                from.w + (count * (to.w - from.w) / 100)
            ), true
        end
        return (timer > duration) and to or from, false
    end

    if UI_RIPPLEBUTTON == nil then
        UI_RIPPLEBUTTON = {}
    end
    if not UI_RIPPLEBUTTON[text] then
        UI_RIPPLEBUTTON[text] = {animation = nil, radius = 5, mouse_coor = nil, time = nil, color = nil}
    end
    local pool = UI_RIPPLEBUTTON[text]
    local radius
   
    if rounding == nil then
        rounding = 0
    end
    if parent_color == nil then
        parent_color = imgui.GetStyle().Colors[imgui.Col.WindowBg]
    end   
    if pool["color"] == nil then
        pool["color"] = imgui.ImVec4(parent_color.x, parent_color.y, parent_color.z, parent_color.w)
    end
    if size == nil then
        local text_size = imgui.CalcTextSize(text:match("(.+)##.+") or text)
        size = imgui.ImVec2(text_size.x + 20, text_size.y + 20)
    end
    if size.x > size.y then
        radius = size.x
        if duration == nil then duration = size.x / 64 end
    else
        radius = size.y
        if duration == nil then duration = size.y / 64 end
    end
    imgui.PushStyleColor(imgui.Col.ChildBg, imgui.GetStyle().Colors[imgui.Col.Button])
    imgui.PushStyleVarVec2(imgui.StyleVar.WindowPadding, imgui.ImVec2(5, 5))
    imgui.PushStyleVarFloat(imgui.StyleVar.ChildRounding, rounding)
    imgui.BeginChild("##ripple effect" .. text, imgui.ImVec2(size.x, size.y), false, imgui.WindowFlags.NoScrollbar)
   
        local draw_list = imgui.GetWindowDrawList()
		if not disa then
        if pool["animation"] and pool["radius"] <= radius * 2.8125 then
            draw_list:AddCircleFilled(pool["mouse_coor"], pool["radius"], imgui.ColorConvertFloat4ToU32(imgui.ImVec4(1, 1, 1, 0.6)), 64)
            pool["radius"] = pool["radius"] + (3 * duration)
            pool["time"] = os.clock()
        elseif pool["animation"] and pool["radius"] >= radius * 2.8125 then
            if bringVec4To(imgui.ImVec4(1, 1, 1, 0.6), imgui.ImVec4(1, 1, 1, 0), pool["time"], 1).w ~= 0 then                  
                draw_list:AddCircleFilled(pool["mouse_coor"], pool["radius"], imgui.ColorConvertFloat4ToU32(imgui.ImVec4(1, 1, 1, bringVec4To(imgui.ImVec4(1, 1, 1, 0.6), imgui.ImVec4(1, 1, 1, 0), pool["time"], 1).w)), 64)
            else
                pool["animation"] = false
            end
        elseif not pool["animation"] and pool["radius"] >= radius * 2.8125 then
            pool["animation"] = false
            pool["radius"] = 5
            pool["time"] = nil
        end 
		end
        if rounding ~= 0  and not disa then               
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y)
            )
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + rounding)
            )
            draw_list:PathArcTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + rounding,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + rounding), rounding, -3, -1.5, 64
            )
           
            draw_list:PathFillConvex(imgui.ColorConvertFloat4ToU32(pool["color"]))
            draw_list:PathClear()
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y)
            )
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x - rounding,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y)
            )
            draw_list:PathArcTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x - rounding,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + rounding), rounding, -1.5, 0, 64
            )
            draw_list:PathFillConvex(imgui.ColorConvertFloat4ToU32(pool["color"]))
            draw_list:PathClear()
           
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y)
            )
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y - rounding)
            )
            draw_list:PathArcTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + rounding,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y - rounding), rounding, 3, 1.5, 64
            )
            draw_list:PathFillConvex(imgui.ColorConvertFloat4ToU32(pool["color"]))
            draw_list:PathClear()
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y)
            )
            draw_list:PathLineTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y - rounding)
            )
            draw_list:PathArcTo(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + size.x - rounding,
                imgui.GetCursorPos().y + imgui.GetWindowPos().y + size.y - rounding), rounding, 0, 1.5, 64
            )
            draw_list:PathFillConvex(imgui.ColorConvertFloat4ToU32(pool["color"]))
            draw_list:PathClear()
        end
        CenterTextFor2Dims(text:match("(.+)##.+") or text)
    imgui.EndChild()
    imgui.PopStyleColor()
    imgui.PopStyleVar(2)
   
   
    imgui.SetCursorPos(imgui.ImVec2(imgui.GetCursorPos().x, imgui.GetCursorPos().y + 10))
    if imgui.IsItemClicked() then
            pool["animation"] = true
            pool["radius"] = 5
            pool["mouse_coor"] = imgui.GetMousePos()
            return true
    end
end

function imgui.CenterText(text)
    imgui.SetCursorPosX(imgui.GetWindowSize().x / 2 - imgui.CalcTextSize(text).x / 2)
    imgui.Text(text)
end

function imgui.OutlineButton(text,size,set)
    if text == nil or type(text) ~= 'string' then imgui.Text('func imgui.OutlineButton(text,size,set); text = nil-not type string ') return end
	imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(230/255, 230/255, 230/255, 1))
    size = size or imgui.ImVec2((imgui.CalcTextSize(text).x),(imgui.CalcTextSize(text).y))
    set = set or {}
    set.Thickness = set.Thickness or 2
    set.Rounding = set.Rounding or 10
    set.Button = set.Button or imgui.GetStyle().Colors[23]
    set.ButtonHovered = set.ButtonHovered or imgui.GetStyle().Colors[24]
    set.Text = set.Text or imgui.GetStyle().Colors[1]

    local p = imgui.GetCursorScreenPos()
    local DL = imgui.GetWindowDrawList()
    local Color = set.Button
    local bool = false

    if imgui.InvisibleButton(text,imgui.ImVec2(size.x,size.y)) then;    bool = true;    end
    if imgui.IsItemHovered() then;    Color = set.ButtonHovered;    end
	imgui.PushFont(font)
    DL:AddText(imgui.ImVec2(p.x+5 - 2,p.y+(size.y/2/2) - 3),imgui.ColorConvertFloat4ToU32(imgui.ImVec4(230/255, 230/255, 230/255, 1)),text)
	imgui.PopFont()
	DL:AddRect(imgui.ImVec2(p.x,p.y), imgui.ImVec2(p.x+(size.x/1.5)+10,p.y+size.y), imgui.ColorConvertFloat4ToU32(imgui.ImVec4(1, 1, 1, 1)), set.Rounding, nil, set.Thickness)
    imgui.PopStyleColor(1)
    return bool
end

function imgui.ButtonWithSettings(text, settings, size)
    imgui.PushStyleVarFloat(imgui.StyleVar.FrameRounding, settings.rounding or imgui.GetStyle().FrameRounding)
    imgui.PushStyleColor(imgui.Col.Button, settings.color or imgui.GetStyle().Colors[imgui.Col.Button])
    imgui.PushStyleColor(imgui.Col.ButtonHovered, settings.color_hovered or imgui.GetStyle().Colors[imgui.Col.ButtonHovered])
    imgui.PushStyleColor(imgui.Col.ButtonActive, settings.color_active or imgui.GetStyle().Colors[imgui.Col.ButtonActive])
    imgui.PushStyleColor(imgui.Col.Text, settings.color_text or imgui.GetStyle().Colors[imgui.Col.Text])
    local click = imgui.Button(text, size)
    imgui.PopStyleColor(4)
    imgui.PopStyleVar()
    return click
end

function imgui.NewOutlineButton(id,text,size,set)
    if text == nil or type(text) ~= 'string' then imgui.Text('func imgui.OutlineButton(text,size,set); text = nil-not type string ') return end
	imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(230/255, 230/255, 230/255, 1))
    size = size or imgui.ImVec2((imgui.CalcTextSize(text).x),(imgui.CalcTextSize(text).y))
    set = set or {}
    set.Thickness = set.Thickness or 2
    set.Rounding = set.Rounding or 10
    set.Button = set.Button or imgui.GetStyle().Colors[23]
    set.ButtonHovered = set.ButtonHovered or imgui.GetStyle().Colors[24]
    set.Text = set.Text or imgui.GetStyle().Colors[1]

    local p = imgui.GetCursorScreenPos()
    local DL = imgui.GetWindowDrawList()
    local Color = set.Button
    local bool = false
	local pp = imgui.GetCursorPos()
	imgui.SetCursorPosY(pp.y + 2)
    if imgui.InvisibleButton(id,imgui.ImVec2((imgui.CalcTextSize(text).x),(imgui.CalcTextSize(text).y+2))) then;    bool = true;    end
	imgui.SetCursorPosY(pp.y)
    if imgui.IsItemHovered() then;    Color = set.ButtonHovered;    end
	imgui.PushFont(font)
    DL:AddText(imgui.ImVec2(p.x+5 - 2,p.y+(size.y/2/2) - 3),imgui.ColorConvertFloat4ToU32(Color),text)
	imgui.PopFont()
    imgui.PopStyleColor(1)
    return bool
end

local mimgui_loader_circle, mimgui_loader_time, mimgui_loader_finish = 1, os.clock(), 0
function mimgui_loader(speed, color)
    if not color then color = 0xFFFFFFFF end
    local draw_list = imgui.GetWindowDrawList()
    local p = imgui.GetCursorScreenPos()
    if mimgui_loader_finish < 1 then
        draw_list:AddCircleFilled(imgui.ImVec2(p.x, p.y), 14.0, (mimgui_loader_circle == 1 and 0x25FFFFFF or color), 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 32, p.y), 14.0, (mimgui_loader_circle == 2 and 0x25FFFFFF or color), 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 64, p.y), 14.0, (mimgui_loader_circle == 3 and 0x25FFFFFF or color), 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 96, p.y), 14.0, (mimgui_loader_circle == 4 and 0x25FFFFFF or color), 30)
        if mimgui_loader_time + speed < os.clock() then  mimgui_loader_time = os.clock()
            if mimgui_loader_circle == 4 then mimgui_loader_circle = 1
            else mimgui_loader_circle = mimgui_loader_circle + 1 end
        end
    elseif mimgui_loader_finish >= 1 then
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + mimgui_loader_finish / 2, p.y), 14.0, color, 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 32 + mimgui_loader_finish, p.y), 14.0, color, 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 64 - mimgui_loader_finish, p.y), 14.0, color, 30)
        draw_list:AddCircleFilled(imgui.ImVec2(p.x + 96 - mimgui_loader_finish / 2, p.y), 14.0, color, 30)
        if mimgui_loader_time + speed < os.clock() then mimgui_loader_time = os.clock()
            if mimgui_loader_finish < 88 then mimgui_loader_finish = mimgui_loader_finish + 1 end
        end
    end
end
-- Gracias gema por las funciones estas
function addnotify(arg,prio)
	if prio == nil then
	prio = 1
	end
	if prio >= 4 then prio = 4 end
	if prio <= 1 then prio = 1 end
	local nombre, razon = arg:match("^(%S+)%s*(.-)$")
	if not nombre or not razon then
		sampAddChatMessage(tag.."Debes proporcionar un nombre y una razon.", 15079712)
		return 
	end
	if not nombre:find("_") then
		sampAddChatMessage(tag.."El nombre debe seguir el formato Nombre_Apellido.", 15079712)
		return 
	end

	if razon == '' then
		for k, v in pairs(notifytable) do
			if v.name:lower() == nombre:lower() then
				removenotify(nombre)
				return
			end
		end
	elseif razon ~= '' then
		for k, v in pairs(notifytable) do
			if v.name:lower() == nombre:lower() then
				sampAddChatMessage(tag.."{e61920}" .. nombre .. "{FFFFFF} ya se encuentra en lista de sospechosos, eliminelo primero.", 16777215)
				return 
			end
		end
	end
	fecha = os.time()
	notifydb = io.open(fileS, "a+")
	io.output(notifydb)
	io.write(nombre .. " " .. razon .. "["..prio.."]["..fecha.."]\n")
	io.close(notifydb)
	table.insert(notifytable, {
		name = nombre:lower(),
		reason = razon,
		priority = prio,
		date = fecha
	})
	updategist()
	sampAddChatMessage(tag.."Has agregado a {e61920}" .. nombre .. "{FFFFFF} a la lista de sospechosos. Razon: " .. razon .. ".", 16777215)
end

function removenotify(arg)
  i = 1
  for k, v in pairs(notifytable) do
    if v.name:lower() == arg then
      table.remove(notifytable, i)
      sampAddChatMessage(tag.."{e61920}" .. mayusculizador(arg) .. "{FFFFFF} ha sido eliminado de la lista de sospechosos.", 16777215)
    end
    i = i + 1
  end
  notifydb = io.open(fileS, "w")
  io.output(notifydb)
  for k, v in pairs(notifytable) do
    io.write(v.name .. " " .. v.reason .. "["..v.priority.."]["..v.date.."]\n")
  end
  io.close(notifydb)
  updategist()
end

function autoupdate(json_url, prefix, url)
    local dlstatus = require('moonloader').download_status
    local json = getWorkingDirectory() .. '\\LISTASAPD\\listasapdV.json'
    if doesFileExist(json) then os.remove(json) end
    downloadUrlToFile(json_url, json,
      function(id, status, p1, p2)
        if status == dlstatus.STATUSEX_ENDDOWNLOAD then
          if doesFileExist(json) then
            local f = io.open(json, 'r')
            if f then
              local info = decodeJson(f:read('*a'))
              updatelink = info.updateurl
              updateversion = info.latest
              f:close()
              os.remove(json)
              if updateversion ~= thisScript().version then
                lua_thread.create(function(prefix)
                  local dlstatus = require('moonloader').download_status
                  local color = -1
                  sampAddChatMessage((prefix..'Actualizacion detectada. Intentando actualizar de la version {82807f}'..thisScript().version..'{FFFFFF} a {82807f}'..updateversion), color)
                  wait(250)
                  downloadUrlToFile(updatelink, thisScript().path,
                    function(id3, status1, p13, p23)
                      if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
                        --print(string.format('Cargado %d de %d.', p13, p23))
                      elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
                        --print('Descarga de la actualizacion {82807f}completada')
                        sampAddChatMessage((prefix..'Actualizacion {82807f}completa!'), color)
                        goupdatestatus = true
                        lua_thread.create(function() wait(500) thisScript():reload() end)
                      end
                      if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                        if goupdatestatus == nil then
                          update = false
                        end
                      end
                    end
                  )
                  end, prefix
                )
              else
                update = false
              end
            end
          else
            update = false
          end
        end
      end
    )
    while update ~= false do wait(100) end
  end

function imgui.DarkTheme()
    imgui.SwitchContext()
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(2, 2)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(0, 0)
    imgui.GetStyle().IndentSpacing = 0
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 10

    --==[ BORDER ]==--
    imgui.GetStyle().WindowBorderSize = 0
    imgui.GetStyle().ChildBorderSize = 1
    imgui.GetStyle().PopupBorderSize = 0
    imgui.GetStyle().FrameBorderSize = 0
    imgui.GetStyle().TabBorderSize = 0

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 10
    imgui.GetStyle().ChildRounding = 5
    imgui.GetStyle().FrameRounding = 5
    imgui.GetStyle().PopupRounding = 5
    imgui.GetStyle().ScrollbarRounding = 5
    imgui.GetStyle().GrabRounding = 5
    imgui.GetStyle().TabRounding = 5

    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().SelectableTextAlign = imgui.ImVec2(0.5, 0.5)
    
    --==[ COLORS ]==--
    imgui.GetStyle().Colors[imgui.Col.Text]                   = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TextDisabled]           = imgui.ImVec4(0.50, 0.50, 0.50, 1.00)
    imgui.GetStyle().Colors[imgui.Col.WindowBg]               = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ChildBg]                = imgui.ImVec4(0.08, 0.08, 0.08, 1.00)
    imgui.GetStyle().Colors[imgui.Col.PopupBg]                = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
    imgui.GetStyle().Colors[imgui.Col.Border]                 = imgui.ImVec4(0.25, 0.25, 0.26, 0.54)
    imgui.GetStyle().Colors[imgui.Col.BorderShadow]           = imgui.ImVec4(0.00, 0.00, 0.00, 0.00)
    imgui.GetStyle().Colors[imgui.Col.FrameBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.FrameBgHovered]         = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
    imgui.GetStyle().Colors[imgui.Col.FrameBgActive]          = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TitleBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TitleBgActive]          = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TitleBgCollapsed]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.MenuBarBg]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ScrollbarBg]            = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ScrollbarGrab]          = imgui.ImVec4(0.00, 0.00, 0.00, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabHovered]   = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabActive]    = imgui.ImVec4(0.51, 0.51, 0.51, 1.00)
    imgui.GetStyle().Colors[imgui.Col.CheckMark]              = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
    imgui.GetStyle().Colors[imgui.Col.SliderGrab]             = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
    imgui.GetStyle().Colors[imgui.Col.SliderGrabActive]       = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
    imgui.GetStyle().Colors[imgui.Col.Button]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ButtonHovered]          = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ButtonActive]           = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
    imgui.GetStyle().Colors[imgui.Col.Header]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.HeaderHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
    imgui.GetStyle().Colors[imgui.Col.HeaderActive]           = imgui.ImVec4(0.47, 0.47, 0.47, 1.00)
    imgui.GetStyle().Colors[imgui.Col.Separator]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.SeparatorHovered]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.SeparatorActive]        = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.ResizeGrip]             = imgui.ImVec4(1.00, 1.00, 1.00, 0.25)
    imgui.GetStyle().Colors[imgui.Col.ResizeGripHovered]      = imgui.ImVec4(1.00, 1.00, 1.00, 0.67)
    imgui.GetStyle().Colors[imgui.Col.ResizeGripActive]       = imgui.ImVec4(1.00, 1.00, 1.00, 0.95)
    imgui.GetStyle().Colors[imgui.Col.Tab]                    = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TabHovered]             = imgui.ImVec4(0.28, 0.28, 0.28, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TabActive]              = imgui.ImVec4(0.30, 0.30, 0.30, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TabUnfocused]           = imgui.ImVec4(0.07, 0.10, 0.15, 0.97)
    imgui.GetStyle().Colors[imgui.Col.TabUnfocusedActive]     = imgui.ImVec4(0.14, 0.26, 0.42, 1.00)
    imgui.GetStyle().Colors[imgui.Col.PlotLines]              = imgui.ImVec4(0.61, 0.61, 0.61, 1.00)
    imgui.GetStyle().Colors[imgui.Col.PlotLinesHovered]       = imgui.ImVec4(1.00, 0.43, 0.35, 1.00)
    imgui.GetStyle().Colors[imgui.Col.PlotHistogram]          = imgui.ImVec4(0.90, 0.70, 0.00, 1.00)
    imgui.GetStyle().Colors[imgui.Col.PlotHistogramHovered]   = imgui.ImVec4(1.00, 0.60, 0.00, 1.00)
    imgui.GetStyle().Colors[imgui.Col.TextSelectedBg]         = imgui.ImVec4(1.00, 0.00, 0.00, 0.35)
    imgui.GetStyle().Colors[imgui.Col.DragDropTarget]         = imgui.ImVec4(1.00, 1.00, 0.00, 0.90)
    imgui.GetStyle().Colors[imgui.Col.NavHighlight]           = imgui.ImVec4(0.26, 0.59, 0.98, 1.00)
    imgui.GetStyle().Colors[imgui.Col.NavWindowingHighlight]  = imgui.ImVec4(1.00, 1.00, 1.00, 0.70)
    imgui.GetStyle().Colors[imgui.Col.NavWindowingDimBg]      = imgui.ImVec4(0.80, 0.80, 0.80, 0.20)
    imgui.GetStyle().Colors[imgui.Col.ModalWindowDimBg]       = imgui.ImVec4(0.00, 0.00, 0.00, 0.70)
end

function sampev.onPlayerJoin(id, color, npc, nombre)
  for k, v in pairs(notifytable) do
    if nombre:lower() == v.name:lower() then
		notf.addNotification("Lista de Sospechosos\n{FFFFFF}"..nombre.." ("..id..") \nSe encuentra ahora en la ciudad.\n \n ", 10, 2)
      return 
    end
  end
end
reasons = {
  [0] = "Crash/Caida de conexion",
  "/Quit | Desconexion voluntaria",
  "Expulsado/Baneado del servidor"
}
function sampev.onPlayerQuit(id, reason)
	local nombre = sampGetPlayerNickname(id)
	for k, v in pairs(notifytable) do
    if nombre:lower() == v.name:lower() then
		notf.addNotification("Lista de Sospechosos\n{FFFFFF}"..nombre.." ("..id..") \n A dejado la ciudad.\n("..reasons[reason]..")\n \n ", 10, 1)
		return 
	end
	end
end

function checknumber(num)
	if type(tonumber(num)) == "number" then
		return true
	else
		return false
	end
end

function isonline(nombre)
	maxPlayerOnline = sampGetMaxPlayerId(false)
	for id = 0, maxPlayerOnline, 1 do
		if sampIsPlayerConnected(id) then
			name = sampGetPlayerNickname(id)
			if name == nombre then
				return id
			end
		end
	end
	return nil
end